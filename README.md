# Toponimia estandarizada

El flujo de trabajo busca generar una nube de puntos a nivel nacional, unificando 
los registros toponímicos detallados disponibles a escalas 100.000 y 25.000 
dispuesto por IGAC y DANE, entidades que entre sus funciones tienen la producción
de información espacial básica.

Los insumo utilizados para esta estandarización fueron consultados el *16 de abril de 2021*,
se listan a continuación:

 - [Toponimia puntual escala 1:100.000 - 2019](https://igacoffice365.sharepoint.com/opendata/Forms/AllItems.aspx?id=%2Fopendata%2Fdata%2FSubdireccion%5FCartografia%2FEscala%5F100%2E000%2FIntegrada%2F2019%2FV1%2F100K%5F2019%5FV1%5Fshp%2Ezip&parent=%2Fopendata%2Fdata%2FSubdireccion%5FCartografia%2FEscala%5F100%2E000%2FIntegrada%2F2019%2FV1&p=true&originalPath=aHR0cHM6Ly9pZ2Fjb2ZmaWNlMzY1LnNoYXJlcG9pbnQuY29tLzp1Oi9nL0VUZ2pnVnlUeG4xUG1ZaGpmcXpsLS1nQl85NnJPbU03amo3bGxxWUNOcThjSnc_cnRpbWU9OE0wUGp5TUIyVWc)
 - [Toponimia puntual escala 1:25.000 - 2017](https://igacoffice365.sharepoint.com/opendata/Forms/AllItems.aspx?id=%2Fopendata%2Fdata%2FSubdireccion%5FCartografia%2FEscala%5F25%2E000%2FIntegrada%2F2017%2FV2%2F25k%5F2017%5FV2%5Fshp%2Ezip&parent=%2Fopendata%2Fdata%2FSubdireccion%5FCartografia%2FEscala%5F25%2E000%2FIntegrada%2F2017%2FV2&p=true&originalPath=aHR0cHM6Ly9pZ2Fjb2ZmaWNlMzY1LnNoYXJlcG9pbnQuY29tLzp1Oi9nL0VXRUE0bXBSLTNsUHRmREw2NEtaMFlFQnlKc2tnR0ZDODdpUnY5MDQtWEYzN3c_cnRpbWU9Y3NOVlBoNEIyVWc)
 - [Nivel Geográfico Manzana Censal 2020](https://geoportal.dane.gov.co/descargas/mgn_2020/MGN2020_URB_MANZANA.rar)
 - [Centros poblados - Geoportal DANE](https://geoportal.dane.gov.co/descargas/divipola/DIVIPOLA_CentrosPoblados.xlsx)
 - [Geovisor de Consulta de Codificación de la Divipola](https://geoportal.dane.gov.co/geovisores/territorio/consulta-divipola-division-politico-administrativa-de-colombia/)
 - [Nivel de referencia veredal 2020](https://geoportal.dane.gov.co/servicios/descarga-y-metadatos/descarga-nivel-de-referencia-de-veredas/)
 - [Nivel de referencia Municipal 2020](https://geoportal.dane.gov.co/servicios/descarga-y-metadatos/descarga-mgn-marco-geoestadistico-nacional/)
 - [Peajes de Colombia 2021](https://inviasopendata-invias.opendata.arcgis.com/datasets/659b88cc326a4251aca01b4f33a870c6_1?geometry=-88.975%2C2.541%2C-59.290%2C10.178)
 


