# Librerías a usar
library(readr)
library(sf)
library(here)
library(dplyr)
library(stringi)
library(data.table)
library(tidyr)
# Limpiar área de trabajo
rm(list = ls())
# Rutas de archivos
rutas<-list(topo=c(here("Ouput/Toponimos.csv")),
            macro=c(here("Datos/shp/Macroterritoriales.shp")),
            urb=c(here("Datos/shp/AreaCensalUrbana.shp")))

# Leer datos
topo<-read_delim(rutas$topo,delim = "|")
macro_mun<-read_sf(rutas$macro)
urbano<-read_sf(rutas$urb)
#
# Cruzar información toponímica con macroterritoriales nivel municipal
#
macro_mun<-macro_mun[,c(1,8,6,3,12)]

# Georreferenciar topónimos para cruzar información
topo<-st_as_sf(topo,coords = c("X","Y"), crs = 4326)
# Spatial Join
topo<-st_join(topo,macro_mun)
topo<-topo[,c(6:10,1:4)]

# Crear columna Tipo (Urbano=1/Rural=0)
urbano<-urbano[,c(13)]
urbano$Tipo<-1

topo<-st_join(topo,urbano)
topo$Tipo[is.na(topo$Tipo)]<-0
# Establecer columna con coordenadas

y<-st_coordinates(topo)
topo<-cbind(topo,y)
topo<-st_drop_geometry(topo)
topo<-topo[!is.na(topo$X),]
topo<-topo[!is.na(topo$Y),]
print(names(topo))
rm(y)


# Nombres en Mayúscula

for (i in c(2,4,7)) { # Columnas nombre: Departamento, Municipio y Topónimos
  topo[[i]]<-toupper(topo[[i]])
}

# Homogeneizar nombres
topo$DPTO_CNMBR[topo$DPTO_CNMBR=="ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA Y SANTA CATALINA"]<-"ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA"
topo$DPTO_CNMBR[topo$DPTO_CNMBR=="BOGOTÁ, D. C."]<-"BOGOTÁ, D.C."
topo$DPTO_CNMBR[topo$DPTO_CNMBR=="QUINDIO"]<-"QUINDÍO"

# Crear columnas para estandarización toponímica
topo$dpto<-topo$DPTO_CNMBR
topo$mun<-topo$MPIO_CNMBR
topo$topo<-topo$NOMBRE_GEO

# Ordenar daros
topo<-topo[,c(1:10,13:15,11,12)]

# Nombres en minúscula
topo_col<-c(names(topo)[11:13]) # Columnas para estandarización toponímica

for (i in topo_col) { # Columnas nombre: Departamento, Municipio y Topónimos
  topo[[i]]<-tolower(topo[[i]])
}

# Quitar tildes
for (i in topo_col) {
  topo[[i]]<-stri_trans_general(topo[[i]],"Latin-ASCII")
}
# Eliminar topónimos que no corresponden a la escala de búsqueda
for (i in c("^departamento","^republica")) {
  topo<-topo[!grepl(i,topo$topo),]  
}

# Términos a buscar en topónimos                              
ter<-c("vereda",
       "corregimiento",
       "parque nacional",
       "consejo comunitario",
       "barrio",
       "comuna",
       "comunidad negra",
       "caserio",
       "resguardo indigena",
       "reserva",
       "inspeccion de policia")
# Agrupar topónimos por categorías
top<-list()

for (i in ter) {
  top[[i]]<-topo[grepl(paste("^",i,sep = ""),topo$topo),]
  top[[i]][["FUENTE"]]<-i
  topo<-topo[!grepl(paste("^",i,sep = ""),topo$topo),]
}

top<-do.call(rbind,top)
topo<-rbind(top,topo)
rm(top,ter)
# Estandarizar categorías de agrupamiento
topo$FUENTE[topo$FUENTE%in% c("c","C")]<-"Corregimiento municipal"
topo$FUENTE[topo$FUENTE%in% c("cp","CP")]<-"CENTRO POBLADO"
topo$FUENTE[topo$FUENTE%in% c("cas","CAS")]<-"caserio"
topo$FUENTE[topo$FUENTE=="CM"]<-"CABECERA MUNICIPAL"
topo$FUENTE[topo$FUENTE=="CD"]<-"Corregimiento Departamenta"
topo$FUENTE[topo$FUENTE=="IP"]<-"inspeccion de policia"
topo$FUENTE[topo$FUENTE=="IPM"]<-"inspeccion de policia municipal"
topo$FUENTE[topo$FUENTE=="IPD"]<-"inspeccion de policia departamenta"
topo$FUENTE[topo$FUENTE=="Vereda DANE"]<-"vereda"

# Eliminar ubicaciones internacionales
paises<-c("VENEZUELA","ECUADOR","BRASIL")
top<-list()
for (i in paises) {
  top[[i]]<-topo [ topo$NOMBRE_GEO==i, ] # Almacenar en lista objetos con nombre "países"
  top[[i]]<-top[[i]] [ !is.na(top[[i]]["MPIO_CCNCT"]), ]  # Identificar objetos con información administrativa
  topo<-topo[topo$NOMBRE_GEO!=i,] # Eliminar objetos de base principal
}

top<-do.call(rbind,top)
topo<-rbind(top,topo)
rm(top,paises)


# Resumen de categorías que agrupan los topónimos
topo_res<-as.data.frame(table(topo$FUENTE,topo$Dato))
topo_res<-topo_res[topo_res$Freq>0,]

# Media geográfica para topónimos repetidos contenidos en un mismo municipio
topo_res<-topo%>%
  group_by(DPTO_CCDGO,
           DPTO_CNMBR,
           MPIO_CCNCT,
           MPIO_CNMBR,
           MacroT,
           CODIGO_VER,
           NOMBRE_GEO,
           dpto,
           mun,
           topo,
           FUENTE,
           Tipo)%>%
  summarise(y=mean(Y),
            x=mean(X))
# Codificación Pseudodane

topo_res<-data.table(topo_res) 
topo_res[, Num_by_Mun := 1:.N, by = "MPIO_CCNCT"] # Requiere "data.table"

#Generar código a partir de conteo por municipio
topo_res <-unite(topo_res,CodCEV,sep="",c(MPIO_CCNCT,Num_by_Mun),remove=FALSE) # Requiere "tidyr"

# Asignación de código DANE a los objetos que ya tenían este atributo asignado por DANE
#topo_res$CodCEV<-ifelse(topo_res$CODIGO_VER!=0,topo_res$CODIGO_VER,topo_res$CodCEV)

#
# Identificar y reparar códigos repetidos
#

# topo_res_rep<-topo_res %>%
#   count(CodCEV) %>%
#   group_by(CodCEV) 
# topo_res_rep<-subset(topo_res_rep,n>1)
# # Identificar datos con código repetido en base maestra
# topo_res_rep2<-merge(topo_res_rep,topo_res,by="CodCEV",all.y = T)
# 
# 
# # Maximo de elementos por municipio
# Mmax<-aggregate(topo_res_rep2$Num_by_Mun, by = list(topo_res_rep2$MPIO_CCNCT), max)
# names(Mmax)<-c("MPIO_CCNCT","NumMax")
# 
# topo_res_rep<-merge(topo_res_rep2,
#                      Mmax,by="MPIO_CCNCT",
#                      all.x=T)
# 
# # Sumar maximos a valores sin código DANE
# topo_res_rep$Num_by_Mun<-ifelse(!is.na(topo_res_rep$n),
#           topo_res_rep$Num_by_Mun + topo_res_rep$NumMax, 
#           topo_res_rep$Num_by_Mun)
# 
# 
# 
# # Asignar nueva codificaión para evitar repeticiones
# topo_res_rep <-unite(topo_res_rep,CodCEV,sep="",c(MPIO_CCNCT,Num_by_Mun),remove=FALSE) # Requiere "tidyr"
# # Asignación de código DANE a los objetos que ya tenían este atributo asignado por DANE
# topo_res_rep$CodCEV<-ifelse(topo_res_rep$CODIGO_VER!=0,
#                             topo_res_rep$CODIGO_VER,
#                             topo_res_rep$CodCEV)
# 
# 
# 
# 
# # Escritura de topónimos estandarizado
# Exportar datos
toponimos<-topo_res[,c(9:13,6,3,14,15)]
names(toponimos)<-c("Departamento",
                    "Municipio",
                    "Topónimo",
                    "Categoria",
                    "Tipo",
                    "MacroT",
                    "CodCev",
                    "y",
                    "x")

# Guardar en formato .csv
write_delim(toponimos,"Ouput/Toponimo_V2.csv",delim = "|")
# Guardar en formato .shp
toponimos<-st_as_sf(toponimos, coords = c("x","y"),crs = 4326)

write_sf(toponimos,"Ouput/Toponimo_V2.shp")
